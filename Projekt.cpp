#include <iostream>
#include <string>

using namespace std;

void wybor_plci();
void wybor_damskich();
void wybor_meskich();
void powrot_zakonczenie(char ktora_plec);

int main()
{
	cout<<"\n \t \t  Witam w programie do szybkiego wyszukiwania butow \n"<< endl;	//tytu�
	
	wybor_plci();
	
	return 0;
}

void wybor_plci()
{
	int plec;	// do wyboru meskich czy damskich
	bool brak_danych=true;	//do petli calosci
	
	cout<<"szukasz butow meskich czy damskich?\n\n1. Meskich\n2. Damskich\n"<< endl;
	
	while (brak_danych==true)	//powtarza jesli wyjdzie ERROR
	{
		cin>>plec;
		
		if (plec==1 || plec==2)
		{
			cout<<"\nJakich dokladniej butow szukasz? (wpisz odpowiedni numer)\n\n";
	
			if (plec==2) 	//buty damskie
			{
				wybor_damskich();
			}
			else if ( plec==1 ) 	//buty meskie
			{
				wybor_meskich();
			}
			
			brak_danych=false;
		}
		else
		{
			cout<<"ERROR (wpisz jeszcze raz)\n";
		}
	}
}

void wybor_damskich()
{
	int rodzaj;	// do wyboru do czego buty
	cout<<"1. Tenisowki\n2. Baleriny\n3. Gorskie\n4. Wodne\n5. Zimowe\n6. Kapcie\n7. Polbuty\n8. Plazowe\n9. Na wysokim obcasie \n\n0. Powrot do poprzedniego wyboru \n"<< endl;
	
	bool nie_gotowe_rodzaje=true; //do petli rodzajow
	while (nie_gotowe_rodzaje==true)	//powtarza jesli wyjdzie ERROR
	{
		cin>>rodzaj;
		
		if (rodzaj>=0 && rodzaj<=9 )
		{
		
			if (rodzaj==1)
			{
				cout<<"\n\t\t Adiidas SUPER STAR\nkolory: bialo-niebieski, ciemnobury, jaskrawy\nrozmiary: 25-48\ncena: 458PLN\n\n"<< endl;
				cout<<"\n\t\t Small Star\nkolory: bialy, czarny\nrozmiary: 34-48\ncena: 280PLN\n\n"<< endl;
				cout<<"\n\t\t CONVERTE\nkolory: brunatny, zielony\nrozmiary: 38-42\ncena: 188PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}						
			else if (rodzaj==2)
			{
				cout<<"\n\t\t Lakierowane\nkolory: czarny\nrozmiary: 36-40\ncena: 68PLN\n\n"<< endl;
				cout<<"\n\t\t Matowe\nkolory: czarny\nrozmiary: 36\ncena: 68PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==3)
			{
				cout<<"\n\t\t Kamieniolamacze\nkolory: zielony, brunatny\nrozmiary: 42-43\ncena: 133PLN\n\n"<< endl;
				cout<<"\n\t\t Kobry\nkolory: kremowy, blado-zolty\nrozmiary: 40-41\ncena: 151PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==4)
			{
				cout<<"\n\t\t Plywaki\nkolory: szary\nrozmiary: 34-38\ncena: 30PLN\n\n"<< endl;
				cout<<"\n\t\t Wodory\nkolory: niebieski\nrozmiary: 28-30\ncena: 20PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==5)
			{
				cout<<"\n\t\t Sniegowce\nkolory: szaro-bialy\nrozmiary: 38-39\ncena: 60PLN\n\n"<< endl;
				cout<<"\n\t\t Yeti\nkolory: czarny\nrozmiary: 38-40\ncena: 40PLN\n\n"<< endl;
				cout<<"\n\t\t Lodowce\nkolory: rozowy\nrozmiary: 20-37\ncena: 16PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==6)
			{
				
				cout<<"\n\t\t Czlapoki\nkolory: ametystowy\nrozmiary: 25-45\ncena: 15PLN\n\n"<< endl;
				cout<<"\n\t\t Kroksy\nkolory: teczowy, czerwony\nrozmiary: 34-48\ncena: 5PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==7)
			{
				cout<<"\n\t\t Polotwarte\nkolory: bialy\nrozmiary: 34-36\ncena: 13PLN\n\n"<< endl;
				cout<<"\n\t\t Polzamkniete\nkolory: czarny\nrozmiary: 38-40\ncena: 14PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==8)
			{
				cout<<"\n\t\t Piaskowe\nkolory: piaskowy\nrozmiary: 34\ncena: 16PLN\n\n"<< endl;
				cout<<"\n\t\t Sandy\nkolory: przezroczyste\nrozmiary: 36\ncena: 17PLN\n\n"<< endl;
				cout<<"\n\t\t Sunshiny\nkolory: zloty\nrozmiary: 48\ncena: 18PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==9)
			{
				cout<<"\n\t\t Szpille\nkolory: czarny\nrozmiary: 34-38\ncena: 156PLN\n\n"<< endl;
				cout<<"\n\t\t Blacharki\nkolory: panterka\nrozmiary: 36-42\ncena: 100PLN\n\n"<< endl;
				cout<<"\n\t\t Dziunie\nkolory: rozowy\nrozmiary: 36\ncena: 200PLN\n\n"<< endl;
				powrot_zakonczenie('k');
			}
			else if (rodzaj==0)
			{
				wybor_plci();
			}
		
			nie_gotowe_rodzaje=false;
		}
		
		else
		{
			cout<<"ERROR (wpisz jeszcze raz)\n";
		}
	}
}

void wybor_meskich()
{	
	int rodzaj;	// do wyboru do czego buty
	cout<<"1. Sportowe\n2. Do tanczenia\n3. Gorskie\n4. Wodne\n5. Zimowe\n6. Kapcie\n7. Do garnituru \n\n0. Powrot do poprzedniego wyboru \n"<< endl;
	
	bool nie_gotowe_rodzaje=true; //do petli rodzajow
	while ( nie_gotowe_rodzaje==true )	//powtarza jesli wyjdzie ERROR
	{
		cin>>rodzaj;
		
		if (rodzaj>=0 && rodzaj<=7)
		{
						
			if (rodzaj==1)
			{
				cout<<"\n\t\t Flashe\nkolory: czerwony\nrozmiary: 40-45\ncena: 100PLN\n\n"<< endl;
				cout<<"\n\t\t Mike\nkolory: czarny, czerwony\nrozmiary: 38-43\ncena: 75PLN\n\n"<< endl;
				cout<<"\n\t\t Adiidas\nkolory: zielony, niebieski, czerwony\nrozmiary: 40-43\ncena: 150PLN\n\n"<< endl;
				powrot_zakonczenie('m');
			}
			else if (rodzaj==2)
			{
				cout<<"\n\t\t Bailandosy Junior\nkolory: czarny, bialy\nrozmiary: 36-42\ncena: 40PLN\n\n"<< endl;
				cout<<"\n\t\t Bailandosy Senior\nkolory: bialy, czarny\nrozmiary: 39-45\ncena: 55PLN\n\n"<< endl;
				powrot_zakonczenie('m');
			}
			else if (rodzaj==3)
			{
				cout<<"\n\t\t Pioniery\nkolory: szary\nrozmiary: 39-45\ncena: 150PLN\n\n"<< endl;
				cout<<"\n\t\t Grotolazy\nkolory: czarny, granatowy\nrozmiary: 36-43\ncena: 95PLN\n\n"<< endl;
				cout<<"\n\t\t McMountain\nkolory: czarno-czerwony, fioletowy\nrozmiary: 42-45\ncena: 180PLN\n\n"<< endl;
				powrot_zakonczenie('m');
			}
			else if (rodzaj==4)
			{
				cout<<"\n\t\t Plywaki\nkolory: niebieski\nrozmiary: 36-44\ncena: 50PLN\n\n"<< endl;
				cout<<"\n\t\t Nurkacze\nkolory: ametystowy\nrozmiary: 43-46\ncena: 65PLN\n\n"<< endl;
				powrot_zakonczenie('m');
			}
			else if (rodzaj==5)
			{
				cout<<"\n\t\t Hoth\nkolory: bialy\nrozmiary: 36-45\ncena: 90PLN\n\n"<< endl;
				powrot_zakonczenie('m');
			}
			else if (rodzaj==6)
			{
				cout<<"\n\t\t Januszki\nkolory: brozowa kratka\nrozmiary: 43-45\ncena: 17PLN\n\n"<< endl;
				cout<<"\n\t\t Czlapoki\nkolory: ametystowy\nrozmiary: 25-45\ncena: 15PLN\n\n"<< endl;
				powrot_zakonczenie('m');
			}
			else if (rodzaj==7)
			{
				cout<<"\n\t\t Marron\nkolory: brozowy\nrozmiary: 39-46\ncena: 175PLN\n\n"<< endl;
				cout<<"\n\t\t Elegante\nkolory: czarny\nrozmiary: 40-47\ncena: 160PLN\n\n"<< endl;
				powrot_zakonczenie('m');
			}
			else if (rodzaj==0)
			{
				wybor_plci();
			}
		
			nie_gotowe_rodzaje=false;
		}
		
		else
		{
			cout<<"ERROR (wpisz jeszcze raz)\n";
		}
	}
	
}

void powrot_zakonczenie(char ktora_plec)
{
	int powrot;	// do wyboru meskich czy damskich
	bool brak_danych=true;	//do petli calosci
	
	cout<<"\n1. Powrot do pierwszego wyboru\n2. Powrot do poprzedniego wyboru\n3. Zakoncz program\n"<< endl;
	
	while (brak_danych==true)	//powtarza jesli wyjdzie ERROR
	{
		cin>>powrot;
		
		if (powrot>=1 && powrot<=3)
		{
			if (powrot==1)
			{
				wybor_plci();
			}
			else if (powrot==2)
			{
				if (ktora_plec=='k')
				{
					wybor_damskich();
				}
				else if (ktora_plec=='m')
				{
					wybor_meskich();
				}
			}
			else if (powrot==3)
			{
				cout<<"\n\t\t Milego Dnia"<< endl;
			}
			
			brak_danych=false;
		}
		else
		{
			cout<<"ERROR (wpisz jeszcze raz)\n";
		}
	}	
}

