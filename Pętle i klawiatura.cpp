#include <iostream>
#include <conio.h>	//biblioteka potrzebna do uzywania KBHIT(VOID) i GETCH()

using namespace std;

int kbhit(void);	//funkcja nacisniecia klawiatury, kbhit -> keyboard hit
int getch();		//funkcja otrzymania znaku, getch -> get char
int main()
{
	float suma=0.0;
	float liczba;
	bool warunek=true;
	
	while(warunek==true)		//petla jest wykonywana dopuki warunek jest spelniony, warunek musi byc typu BOOL (2 wartosci)
	{
		cout <<"podaj liczbe\n0-obliczenie sumy"<< endl;
		cin>>liczba;
		suma=suma+liczba;
		if(liczba==0) 	//wpisanie 0 powoduje zmiane wartosci zmiennej WARUNEK co przerywa petle
			warunek=false;
	}
	cout<<"\n"<<suma<< endl;
	
	
	
	while(!kbhit())		// warunek: jesli nie ( !-negacja ) nasisnieta klawiatura
	{
		cout<<"M  A  T  R  I  X  ";	
	}
	
	
	
	
	int x=getch();
	while(x!='s'&& x!='S')	// warunek: jesli x nie jest s i S	UWAGA/WARNING/ACHTUNG/ADVERTENCIA/ATTENTION: tam musi byc pojedynczy cudzyslow
		{
			x=getch();	//podobna to komenty CIN ale pobiera tylko 1 znak
			cout<<"dziala ";
		}



		
	int z;
		
	do	//petla DO WHILE dziala tak ze najpierw wykona petle raz a potem jesli warunek sie zgadza wykona ja ponownie itd.
	{
		z=getch();
		cout<<"dziala ";
	}
	while(z!='s'&& z!='S');		//rozni sie od WHILE tym ze DO WHILE wykona polecenia przynajmniej raz, a WHILE moze nie wykonac ani razu (jesli warunek nie bedzie spelniony)
}
