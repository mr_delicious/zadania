#include <iostream>
using namespace std;

const int TAB_ROZMIAR=10;
int tab[TAB_ROZMIAR];
bool zmiana=false;

void wyswietl();
void wypelnianie();
void sortowanie();

int main()
{
	wypelnianie();
	do
	{
		zmiana=false;
		sortowanie();
		if(zmiana)
			wyswietl();
		
	} while(zmiana==true);
	
	
	return 0;
}

void wypelnianie()
{
	for(int i=0;i<TAB_ROZMIAR;i++)
	{
		cout<<"\nprosze podac wartosc dla indeksu nr "<<i<<endl;
		cin>>tab[i];
	}
	cout<<"\ntablica:"<<endl;
	wyswietl();

}

void sortowanie()
{
	for(int t=0;t<TAB_ROZMIAR-1;t++)
	{
		if(tab[t]>tab[t+1])
		{
			int pamiec=tab[t];
			tab[t]=tab[t+1];
			tab[t+1]=pamiec;
			zmiana=true;
		}
	}
}

void wyswietl()
{
	cout<<"\n"<<endl;
	for(int x=0; x<TAB_ROZMIAR;x++)
	{
		cout<<tab[x]<<"\t";
	}
}
