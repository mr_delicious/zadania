#include <iostream>

using namespace std;

int main()
{
	int numer_dnia_tygodnia;
	cout<<"Podaj numer dnia tygodnia\n";
	cin>>numer_dnia_tygodnia;
	switch(numer_dnia_tygodnia)	//zmienna tylko typu calkowitego
	{
		case 1:
			cout<<"\nponiedzialek"<<endl;
			cout<<"jest pierwszym dniem tygodnia"<<endl;	//nie trzeba dawac nawiasu zeby wykonac kilka polecen (w przeciwienstwie do IF)
		case 2:
			cout<<"\nwtorek"<<endl;
		case 3:
			cout<<"\nsroda"<<endl;
		case 4:
			cout<<"\nczwartek"<<endl;
		case 5:
			cout<<"\npiatek"<<endl;
		case 6:
			cout<<"\nsobota"<<endl;
		case 7:
			cout<<"\nniedziela"<<endl;
		default:
			cout<<"\nERROR"<<endl;
	}									//w takiej konstrukcji program sprawdza od gory warunek i jesli trafi na wlasciwy warunek to wykona wszystkie polecenia ponizej
	
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	
	int numer_dnia_tygodnia1;
	cout<<"\n\nPodaj numer dnia tygodnia\n";
	cin>>numer_dnia_tygodnia1;
	switch(numer_dnia_tygodnia1)	//zmienna tylko typu calkowitego
	{
		case 1:
			cout<<"\nponiedzialek"<<endl;
			cout<<"jest pierwszym dniem tygodnia"<<endl;
			break;
		case 2:
			cout<<"\nwtorek"<<endl;
			break;
		case 3:
			cout<<"\nsroda"<<endl;
			break;
		case 4:
			cout<<"\nczwartek"<<endl;
			break;
		case 5:
			cout<<"\npiatek"<<endl;
			break;
		case 6:
			cout<<"\nsobota"<<endl;
			break;
		case 7:
			cout<<"\nniedziela"<<endl;
			break;
		default:						//DEFAULT wykona sie w przypadku kiedy wszystkie powyzsze CASE nie beda spelnione
			cout<<"\nERROR"<<endl;
			break;
	}									//w takiej konstrukcji program wykonuje tylko te polecenia ktore sa miedzy CASE z odpowiednim warunkiem a BREAK (nie bedzie sprawdzac pozostalych wtedy)
	
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

	int x;
	cin>>x;
	switch(x)
	{
		case 1:
			cout<<"x";	//bez ENDL nastepny wyswietlony lub wpisany tekst bedzie w tej samej linijce
		case 2:			//przy braku BREAK przy wybraniu 1 zostana wykonane wszystkie polecenia do najblizszebo BREAK
			cout<<"x";	//dlatego przy 1 wynik jest taki sam jak przy 3
			break;
		case 3:
			cout<<"xx";
			break;
		default:
			cout<<"ERROR";
			break;
	}
	

}
