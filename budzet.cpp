#include <iostream>
using namespace std;

float budzet[12][4];
float przychody=0.0;
float wydatki=0.0;
float suma=0;
int miesiac=0;

float oszczednosci(float a,float b)
{
    float c=0;
    if(a>b)
        c=a-b;
    else
        c=0;
        
    budzet[miesiac][2]=c;
   return 0; 
}

float pozyczka(float f,float g)
{
    float h=0;
    if(f<g)
        h=g-f;
    else
        h=0;
        
    budzet[miesiac][3]=h;
   return 0; 
}


int main()
{
    int x=0;
    do
    {
        cout<<"wpisz numer miesiaca"<<endl;
        cin>>miesiac;
        
        if(miesiac>0 && miesiac<13)
        {
        	--miesiac;
            cout<<"\npodaj przychod w tym miesiacu"<<endl;
            cin>>przychody;
            budzet[miesiac][0]=przychody;
        
            cout<<"\npodaj wydatki w tym miesiacu"<<endl;
            cin>>wydatki;
            budzet[miesiac][1]=wydatki;
            
            oszczednosci(przychody, wydatki);
            pozyczka(przychody, wydatki);
        }
        else
            cout<<"error"<<endl;
        
        cout<<"wybierz: 0-kolejny miesiac, 1-przejdz dalej"<<endl;
        cin>>x;
        miesiac=0;
        przychody=0;
        wydatki=0;
    } while(x==0);
    
    cout<<"mies\t\tprzychody\t\twydatki\t\toszczednosci\t\tpozyczka"<<endl;
    int zz=1;
    for(int z=0; z<12; z++)
    {
    	cout<<zz<<"\t\t"<<budzet[z][0]<<"\t\t\t"<<budzet[z][1]<<"\t\t\t"<<budzet[z][2]<<"\t\t\t"<<budzet[z][3]<<endl;
    	zz++;
    }
    
    int wybor=0;
    cout<<"\nwybierz: 1-suma oszczednosci,2-suma pozyczek,0-zakoncz program"<<endl;
    cin>>wybor;
    switch(wybor)
    {
        case 1:
            for(int u=0;u<12;u++)
                {
                    suma=suma+budzet[u][2];
                }
            cout<<"\nsuma oszczednosci="<<suma<<endl;
            break;
        case 2:
            for(int w=0;w<12;w++)
                {
                    suma=suma+budzet[w][3];
                }
            cout<<"\nsuma pozyczek="<<suma<<endl;
            break;
        default:
            break;
    }
    
    
    return 0;
}
