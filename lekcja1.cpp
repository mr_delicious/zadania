#include <iostream>
#include <string>
#include <limits>

using namespace std;
int main()
{
	//BOOL ma wartosci tylko 0 i 1 !!!
	//znaki rownosci, mniejszosci, wiekszosci i nierownosci
cout<<"1-prawda 0-falsz"<< endl;
int x=10;
int y=5;
bool wynik=0;
cout<<"x="<<x<<"\t y="<<y<< endl;
wynik=x!=y;
cout <<"x nie jest rowne y \t"<<wynik<< endl;
wynik=x==y;
cout <<"x jest rowne y \t"<<wynik<< endl;
wynik=x>=y;
cout <<"x jest rowne lub wieksze od y \t"<<wynik<< endl;
wynik=x<=y;
cout <<"x jest rowne lub mniejsze od y \t"<<wynik<< endl;
wynik=x>y;
cout <<"x jest wieksze od y \t"<<wynik<< endl;
wynik=x<y;
cout <<"x jest mniejsze od y \t"<<wynik<<"\n \n" <<endl;


	// ! to negacja czyli 0 na 1, a 1 na 0

int a=5;
int b=4;
bool Wynik=0;
cout <<"a="<<a<<"\t b="<<b<< endl;
wynik=a>b;
cout <<"a jest wieksze od b \t"<< wynik << endl;
wynik=!Wynik;
cout <<"negacja wyniku "<<Wynik<<"\n \n"<< endl; 


	// || znaczy ze kiedy choc jedna wartosc jest rowna 1 to wynik jest rowny 1 (dodawanie)

bool A=0;
bool B=0;
wynik=0;

wynik=A||B;
cout <<"A="<<A<<"\t B="<<B<<"\t Wynik="<< wynik << endl;

a=1;
wynik=a||b;
cout <<"A="<<A<<"\t B="<<B<<"\t Wynik="<< wynik << endl;

a=0;
b=1;
wynik=a||b;
cout <<"A="<<A<<"\t B="<<B<<"\t Wynik="<< wynik << endl;

a=1;
wynik=a||b;
cout <<"A="<<A<<"\t B="<<B<<"\t Wynik="<< wynik <<"\n \n"<< endl;


	// && znaczy ze musza byc same 1 zeby wynik byl 1, jesli bedzie choc jedno 0 to wynik bedzie 0 (mnozenie)

A=0;
B=0;
wynik=0;

wynik=a&&b;
cout <<"a="<<a<<"\t b="<<b<<"\t wynik="<< wynik << endl;

a=1;
wynik=a&&b;
cout <<"a="<<a<<"\t b="<<b<<"\t wynik="<< wynik << endl;

a=0;
b=1;
wynik=a&&b;
cout <<"a="<<a<<"\t b="<<b<<"\t wynik="<< wynik << endl;

a=1;
wynik=a&&b;
cout <<"a="<<a<<"\t b="<<b<<"\t wynik="<< wynik <<"\n \n"<< endl;


	// && ma pierwszenstwo przed || (najpierw mnozenie potem dodawanie)

A=0;
B=0;
bool c=1;
bool d=1;
wynik=0;

wynik= a||b&&c||d; // 0+0*1+1
cout <<wynik<<"\n \n"<< endl;


	// operatory przypisania
int q=5;
cout <<"q="<<q<< endl;

q=5;
q+=5;
cout <<"q+="<<q<< endl; //q+=5 oznacza q=q+5

q=5;
q-=5;
cout <<"q-="<<q<< endl; //q-=5 oznacza q=q-5

q=5;
q*=5;
cout <<"q*="<<q<< endl; //q*=5 oznacza q=q*5

q=5;
q/=5;
cout <<"q/="<<q<< endl; //q/=5 oznacza q=q/5

q=5;
q%=5;
cout <<"q%="<<q<<"\n \n"<< endl; //q%=5 oznacza q=q%5

q=5;
cout <<"q="<<q<< endl;
//inkrementacja
q++; //oznacza q+1
cout <<"q++ to "<<q<< endl;

q=5;
//dekrementacja
q--; //oznacza q-1
cout << "q-- to "<<q<<"\n \n"<< endl;

	//inkrementacja
q=5;
cout << q << endl;
cout << q++ << endl; //najpierw wyswietla q a potem zwieksza o 1
cout << q<<"\n \n"<< endl; //teraz juz q jest zwiekszone

	//postinkrementacja
q=5;
cout << q << endl;
cout << ++q << endl; //najpierw zwieksza q o 1 a potem wyswietla
cout << q <<"\n \n"<< endl;

return 0;
}
