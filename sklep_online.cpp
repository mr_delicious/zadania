#include <iostream>
#include <iomanip>
#include <typeinfo>
#include <cstdio>
#include <time.h>
#include <cstdlib>
#include <cstring>

using namespace std;

// deklaracja struktury
struct produkt {
    string nazwa;
    int id_nazwy;
    int cena;
    int ilosc;
};


int main()
{
    setlocale(LC_ALL, "");		// włączenie wyświetlania polskich znaków
    produkt chleb; // tworzenie obiektu struktury o nazwie chleb
    chleb.nazwa = "chleb";
    chleb.id_nazwy =1;
    chleb.cena = 3;
    chleb.ilosc = 6;
    
    produkt parowki; // tworzenie obiektu struktury o nazwie parowki
    parowki.nazwa = "parówka";
    parowki.id_nazwy =2;
    parowki.cena = 5;
    parowki.ilosc = 20;
    
    produkt makaron ; // tworzenie obiektu struktury o nazwie makaron
    makaron.nazwa = "makaron";
    makaron.id_nazwy =3;
    makaron.cena = 9;
    makaron.ilosc = 15;
    
    produkt maka  ; // tworzenie obiektu struktury o nazwie maka
    maka.nazwa = "mąka";
    maka.id_nazwy =4;
    maka.cena = 6;
    maka.ilosc = 34;
    
    produkt lody_karmelowe  ; // tworzenie obiektu struktury o nazwie lody_karmelowe
    lody_karmelowe.nazwa  = "lody karmelowe";
    lody_karmelowe.id_nazwy =5;
    lody_karmelowe.cena = 10;
    lody_karmelowe.ilosc = 8;
    
    cout<<"\t\tPRODUKTY\nnazwa\t\t\tcena\tilość\n"<<endl;
    cout<<chleb.nazwa<<"\t\t\t"<<chleb.cena<<"\t"<<chleb.ilosc<<endl;
    cout<<parowki.nazwa<<"\t\t\t"<<parowki.cena<<"\t"<<parowki.ilosc<<endl;
    cout<<makaron.nazwa<<"\t\t\t"<<makaron.cena<<"\t"<<makaron.ilosc<<endl;
    cout<<maka.nazwa<<"\t\t\t"<<maka.cena<<"\t"<<maka.ilosc<<endl;
    cout<<lody_karmelowe.nazwa<<"\t\t"<<lody_karmelowe.cena<<"\t"<<lody_karmelowe.ilosc<<endl;
    
    int koszyk [5] [2]={{1,0},{2,0},{3,0},{4,0},{5,0}};
    
    cout<<"\nWybierz towar, wpisz 9 aby podliczyć cenę i zakończyć program lub 8 aby wyświetlić koszyk"<<endl;
    cout<<"1- "<<chleb.nazwa<<endl;
    cout<<"2- "<<parowki.nazwa<<endl;
    cout<<"3- "<<makaron.nazwa<<endl;
    cout<<"4- "<<maka.nazwa<<endl;
    cout<<"5- "<<lody_karmelowe.nazwa<<endl;
    cout<<"\n(w przypadku podania większej ilości niż jest dostępna wpisana zostanie maksymalna ilość)"<<endl;
    int wybor=0;
    int Ilosc=0;
    
    while(true)
    {
        cin>>wybor;
        if(wybor==9)
            break;
        if(wybor==8)
        {
            for(int i=0; i<5; i++)
            {
                if(koszyk[i][0]==1)
                    cout<<chleb.nazwa;
                else if(koszyk[i][0]==2)
                    cout<<parowki.nazwa;
                else if(koszyk[i][0]==3)
                    cout<<makaron.nazwa;
                else if(koszyk[i][0]==4)
                    cout<<maka.nazwa;
                else if(koszyk[i][0]==5)
                    cout<<lody_karmelowe.nazwa;
                cout<<": "<<koszyk[i][1]<<" szt."<<endl;
            }
        }
        else
        {
            cout<<"Wpisz ilość (wpisz ujemną żeby odjąć produkty)"<<endl;
            cin>>Ilosc;
            if(wybor==1)
            {
                koszyk[wybor-1][1]+=Ilosc;
                if(koszyk[wybor-1][1]>chleb.ilosc)
                    koszyk[wybor-1][1]=chleb.ilosc;
            }
            else if(wybor==2)
            {
                koszyk[wybor-1][1]+=Ilosc;
                if(koszyk[wybor-1][1]>parowki.ilosc)
                    koszyk[wybor-1][1]=parowki.ilosc;
            }
            else if(wybor==3)
            {
                koszyk[wybor-1][1]+=Ilosc;
                if(koszyk[wybor-1][1]>makaron.ilosc)
                    koszyk[wybor-1][1]=makaron.ilosc;
            }
            else if(wybor==4)
            {
                koszyk[wybor-1][1]+=Ilosc;
                if(koszyk[wybor-1][1]>maka.ilosc)
                    koszyk[wybor-1][1]=maka.ilosc;
            }
            else if(wybor==5)
            {
                koszyk[wybor-1][1]+=Ilosc;
                if(koszyk[wybor-1][1]>lody_karmelowe.ilosc)
                    koszyk[wybor-1][1]=lody_karmelowe.ilosc;
            }
            else
                cout<<"ERROR"<<endl;
            if(koszyk[wybor-1][1]<0)
                koszyk[wybor-1][1]=0;
            cout<<"koszyk pomyślnie zaktualizowany!"<<endl;
        }
    }
    float koszt=0;
    koszt+=chleb.cena*koszyk[0][1];
    koszt+=parowki.cena*koszyk[1][1];
    koszt+=makaron.cena*koszyk[2][1];
    koszt+=maka.cena*koszyk[3][1];
    koszt+=lody_karmelowe.cena*koszyk[4][1];
    cout<<"koszt całości wynosi "<<koszt<<"zł"<<endl;
    
    return 0;
}