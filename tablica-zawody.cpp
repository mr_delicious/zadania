#include <iostream>

using namespace std;

int zawodnicy;
int konkurencje;
int y=0;
int punkty=0;
int suma=0;
int wybor=0;
int maksimum=0;
int minimum=10000000;

int main()
{
	cout<<"podaj ilosc zawodnikow"<<endl;
	cin>>zawodnicy;
	cout<<"\npodaj ilosc konkurencji"<<endl;
	cin>>konkurencje;

	int tab[zawodnicy][konkurencje];
	
	for(int x=0;x<zawodnicy;x++)
	{
		cout<<"Prosze podac wartosc punktow dla zawodnika nr "<<x+1;
		do
		{
			cout<<"\nw konkurencji nr "<<y+1<<":"<<endl;
			cin>>punkty;
			tab[x][y]=punkty;
			y++;
			
			if(punkty==0)
			{
				while(y<konkurencje)
				{
					tab[x][y]=0;
					y++;
				}
				break;
			}
				
		punkty=0;
			
		} while(y<konkurencje);
		
		y=0;
	}
	
	for(int z=0; z<zawodnicy; z++)
    {
    	cout<<"\nnr"<<z+1<<"|\t";
		for(int k=0;k<konkurencje;k++)
		{
			cout<<tab[z][k]<<"\t";
		}
    }
    
	cout<<"\n\n\t\tSUMY PUNKTOW"<<endl;
	
	for(int g=0;g<zawodnicy;g++)
	{
		cout<<"zawodnik nr"<<g+1<<": ";
		
		for(int h=0;h<konkurencje;h++)
		{
			suma=suma+tab[g][h];
		}
		
		if(tab[g][konkurencje-1]==0)
			cout<<"DYSKWALIFIKACJA"<<endl;
		else
			{
				cout<<suma<<"pkt"<<endl;
				if(suma>maksimum)
					maksimum=suma;
				if(suma<minimum)
					minimum=suma;
			}
				
		suma=0;
	}
    
    cout<<"\n\nwybierz: 1-najmniejsza suma, 2-najwieksza suma, inny-koniec programu"<<endl;
    cin>>wybor;
    
    switch(wybor)
    {
        case 1:
            cout<<"\nnajmniejsza suma to: "<<minimum<<"pkt"<<endl;
            break;
        case 2:
            cout<<"\nnajwieksza suma to: "<<maksimum<<"pkt"<<endl;
            break;
        default:
            break;
    }
    return 0;
}

